""" URL config for semety project. """

from django.conf import settings
from django.conf.urls import include
from django.urls import path
from django.contrib import admin
from django.views import generic
from django.conf.urls.static import static
from .utils.views import FrontendView
from .apps.site.views import Homepage, WelcomePage, FeebackForm, About, Legal, Contact, Explore
from semety.apps.core.views import signup

admin.autodiscover()

from django.contrib.contenttypes.views import shortcut
from django.contrib.auth.views import password_change_done


urlpatterns = [

    # Admin URLs.
    path('admin/password_change/done/', password_change_done, name='password_change_done'),
    path('admin/', admin.site.urls),
    path('register/', signup, name="register"),
    path('', include('social.apps.django_app.urls', namespace='social')),
    path('', include(('django.contrib.auth.urls', 'auth'), namespace='auth')),
    path('feedback/', FeebackForm.as_view(), name='login'),

    path('about/', About.as_view(), name='about'),
    path('legal/', Legal.as_view(), name='legal'),
    path('contact/', Contact.as_view(), name='contact'),
    path('explore/', Explore.as_view(), name='explore'),

    #Homepage
    path('', Homepage.as_view()),
    path('welcome/', WelcomePage.as_view()),
    path('', include('semety.apps.workspace.urls', namespace='workspace')),
    path('settings/', include('semety.apps.core.urls', namespace='core')),
    path('network/', include('semety.apps.network.urls', namespace='network')),
    path('api/', include('semety.apps.api.urls', namespace='workspace')),
    #path('network/', include('semety.apps.teams.urls', namespace='team')),
    path('clients/', include('semety.apps.clients.urls', namespace='clients')),
    path('notifications/', include('semety.apps.notifications.urls', namespace='notifications')),
    path('site/', include('semety.apps.site.urls', namespace='site')),
    #path('', include('semety.apps.teams.urls', namespace='teams')),


    # Permalink redirection service.
    path('<slug:content_type_id>-<slug:object_id>/', shortcut, name='permalink_redirect'),


    path('favicon.ico', generic.RedirectView.as_view(permanent=True)),
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    urlpatterns += [
        path('404/', generic.TemplateView.as_view(template_name='404.html')),
        path('500/', generic.TemplateView.as_view(template_name='500.html')),
        path('frontend/', FrontendView.as_view()),
        path('frontend/<slug:slug>/', FrontendView.as_view())
    ]

