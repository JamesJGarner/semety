$(document).foundation();

$('input').on("click", function() {
    window.onbeforeunload = null;
})

/* icon finder */
function icon_init(element_id) {
    $('#id_icon', element_id).iconpicker();
    $(window).on('load', function() {
        var icon_val = $('#id_icon', element_id).val()
        $('#id_icon', element_id).hide();
        $('#icon_display i', element_id).addClass(icon_val);
    });


    $(window).on('load', function() {
        $(".iconpicker-items a", element_id).on('click', function() {
            var result = $(this).find("i").attr('class');
            $('#icon_display i', element_id).removeClass().addClass(result)
        });
    });

}

$('#notification-button').on('click', function(e){
   e.preventDefault();
   $('.notifications').toggleClass('open').focus();
})

$('.notifications').on('focusout', function () {
    console.log('test')
  $('.notifications').removeClass('open');
});

$(document).click(function(event) {
    if(!$(event.target).closest('#notification-button').length) {
        if(!$(event.target).closest('.notifications').length) {
            $('.notifications').removeClass('open');
        }
    }
});
