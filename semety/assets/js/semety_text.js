// Enable navigation prompt
function enableBeforeUnload() {
    window.onbeforeunload = function (e) {
        return "Discard changes?";
    };
}

function disableBeforeUnload() {
    window.onbeforeunload = null;
}

function semetyText(window_div) {

    var window_div = $(window_div);
    console.log(window_div)
    var box = $("#text-area-display", window_div);

    $('#underline', window_div).on('click', function (e) {
        e.preventDefault()
        box.focus();
        document.execCommand('underline', false, null);
    });

    $('#italic', window_div).on('click', function (e) {
        e.preventDefault()
        box.focus();
        document.execCommand('italic', false, null);
    });

    $('#orderedlist', window_div).on('click', function (e) {
        e.preventDefault()
        box.focus();
        document.execCommand("insertorderedlist");
    });

    $('#unorderedlist', window_div).on('click', function (e) {
        e.preventDefault()
        box.focus();
        document.execCommand("insertUnorderedList");
    });

    $('#bold', window_div).on('click', function (e) {
        e.preventDefault();
        box.focus();
        document.execCommand(this.id, false, null);
    });

    $('#h1', window_div).on('click', execCommand);
    $('#h2', window_div).on('click', execCommand);
    $('#h3', window_div).on('click', execCommand);
    $('#h4', window_div).on('click', execCommand);
    $('#h5', window_div).on('click', execCommand);
    $('#h6', window_div).on('click', execCommand);

    $('#full-screen', window_div).on('click', toggleFullscreen);


    function toggleFullscreen(e) {
        e.preventDefault();
        console.log(window_div);
        window_div.toggleClass('open');

    }

    function execCommand(e) {
        e.preventDefault();
        box.focus();
        document.execCommand('formatBlock', false, '<' +  this.id +'>');
        $('.dropdown-options', window_div).removeClass("show");
    }

    $('.dropdown-tool', window_div).on('click', toggleHeadings);


    function toggleHeadings() {
        $('.dropdown-options', window_div).toggleClass("show");
    }

    $('.add-link-overlay', window_div).on('click', function(e) {
        e.preventDefault();

        $('.add-link-overlay', window_div).addClass('hidden');
    })

    $('.add-link').on('click', function(e) {
        e.stopPropagation();
    })

    $('#link', window_div).on('click', function(e) {
      e.preventDefault()
      console.log(window_div)
      $('.add-link-overlay', window_div).toggleClass('hidden');
    });

    $('.close-button', window_div).on('click', function(e) {
         $('.add-link-overlay', window_div).addClass('hidden');
    });

    $('#add-link', window_div).on('click', function (e) {
        e.preventDefault()
        var selected = $('#link-text', window_div).val()
        var href = $('#link-url', window_div).val()
        box.focus();
        document.execCommand("insertHTML",false,"<a href='"+href+"'>"+selected+"</a>");
        $('.add-link-overlay', window_div).addClass('hidden');
        var selected = $('#link-text', window_div).val("");
        var href = $('#link-url', window_div).val("");
    });


    //parentElement div.input_display.editable

    function replaceSelectedText(replacementText) {
        var sel, range;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();
                range.insertNode(document.createTextNode(replacementText));
            }
            sel
        } else if (document.selection && document.selection.createRange) {
            range = document.selection.createRange();
            range.text = replacementText;
        }
    }

    $("#text-area-display", window_div).bind('input', function() {
        enableBeforeUnload()
        $("#id_text", window_div).val($(this).html())

    });

    $(window).on('load', function() {
        window.onbeforeunload = null;
        var value = $("#id_text", window_div).val();
        $("#text-area-display", window_div).html(value);
    });


    function handlePaste (e) {
        var clipboardData, pastedData;
        e.stopPropagation();
        e.preventDefault();
        var clipboardData =  window.event.clipboardData.getData('text');
        insertTextAtCursor(clipboardData)

    }

    $("#text-area-display", window_div).on('paste', function (e) {
        handlePaste(e)
        $("#id_text", window_div).val($("#text-area-display", window_div).html())
    });

    function insertTextAtCursor(text) {
        var sel, range, html;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();
                range.insertNode( document.createTextNode(text) );
            }
        } else if (document.selection && document.selection.createRange) {
            document.selection.createRange().text = text;
        }
    }


    $("#text-area-display", window_div)
        .bind('dragenter',function(event){
            //console.log('dragenter');
            event.preventDefault();
        })
        .bind('dragleave',function(event){
            //console.log('dragleave');
        })
        .bind('dragover',function(event){
            //console.log('dragover');
            event.preventDefault();
        })
        .bind('drop',function(event){
            event.preventDefault();

        })
        .bind('dragend',function(event){
            //console.log('dragend');
        });
}
