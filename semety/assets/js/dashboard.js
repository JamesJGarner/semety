function run_meter(meter) {
        var value = parseInt($(meter).attr('data-width'));

        $(meter).delay(200).animate({
            width: value+"%",
        }, 400);
    };


$( document ).ready(function() {
    run_meter("#your_product");
    run_meter("#your_temas_product");
});
