"""
Settings for local development.

These settings are not fast or efficient, but allow local servers to be run
using the django-admin.py utility.

This file should be excluded from version control to keep the settings local.
"""

import os
import os.path

from .base import *  # pylint: disable=unused-wildcard-import,wildcard-import

# Run in debug mode.

DEBUG = True

TEMPLATE_DEBUG = DEBUG


# Save media files to the user's Sites folder.

MEDIA_ROOT = os.path.expanduser(os.path.join("~/Sites", SITE_DOMAIN, "media"))
STATIC_ROOT = os.path.expanduser(os.path.join("~/Sites", SITE_DOMAIN, "static"))
NODE_MODULES_ROOT = os.path.expanduser(os.path.join("~/Workspace/semety", "node_modules"))


# Use local server.

SITE_DOMAIN = "localhost:8000"

PREPEND_WWW = False


ALLOWED_HOSTS = [
    SITE_DOMAIN,
    "localhost",
    "http://locahost:8000",
    "*",
]

THUMBNAIL_DEBUG = True

# Disable the template cache for development.

TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
)


# Optional separate database settings
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "HOST": "localhost",
        "NAME": "semety3",
        "USER": "postgres",
        "PASSWORD": "ladoftheday"
    },
}

# Mailtrip SMTP

EMAIL_HOST = 'smtp.mailtrap.io'
EMAIL_HOST_USER = 'd750e18bf7e87c'
EMAIL_HOST_PASSWORD = 'f31cc759268bfc'
EMAIL_PORT = '2525'
