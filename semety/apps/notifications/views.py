from django.views.generic import ListView
from .models import Notification
from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponseRedirect

class NotificationList(ListView):
    model = Notification


def NotificationRead(request, pk):
    notification = get_object_or_404(
        Notification, user=request.user, id=pk)
    notification.read = True
    notification.save(send_email=False)

    return redirect(notification.model_instance.get_absolute_url())


def NotifcationMarkAllAsRead(request):
    for notifcation in Notification.objects.filter(user=request.user):
        notifcation.read = True
        notifcation.save(send_email=False)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
