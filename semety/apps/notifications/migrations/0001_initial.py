# Generated by Django 2.0.6 on 2018-08-12 12:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('workspace', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('read', models.BooleanField(default=False)),
                ('model', models.CharField(blank=True, choices=[('workspace_thread', 'Thread'), ('workspace_reply', 'Reply')], max_length=200, null=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('workspace_reply', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='workspace.Reply')),
                ('workspace_thread', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='workspace.Thread')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
