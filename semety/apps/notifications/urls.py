from django.urls import path
from .views import *

app_name = 'notifications'

urlpatterns = [
    path('', NotificationList.as_view(), name='notification_list'),
    path('<int:pk>', NotificationRead, name="notification_view"),
    path('mark-all-as-read/', NotifcationMarkAllAsRead, name="read-all"),
]
