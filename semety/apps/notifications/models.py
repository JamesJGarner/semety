from django.db import models
from ..workspace.models import Thread, Reply
from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.urls import reverse

User = get_user_model()

MODEL_TYPES = (
    ('workspace_thread', 'Thread'),
    ('workspace_reply', 'Reply'),
)

class ModalsTypes(models.Model):

    workspace_thread = models.ForeignKey(
        Thread,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    workspace_reply = models.ForeignKey(
        Reply,
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    class Meta:
        abstract = True,


class Notification(ModalsTypes):

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE
    )

    text = models.TextField(
    )

    datetime = models.DateTimeField(
        auto_now_add=True,
    )

    read = models.BooleanField(
        default=False,
    )

    model = models.CharField(
        choices=MODEL_TYPES,
        max_length=200,
    )

    icon = models.CharField(
        max_length=200,
        null=True,
    )


    @models.permalink
    def get_absolute_url(self):
        return ("notifications:notification_view", {
            self.pk,
        })


    class Meta:
        ordering = ('-datetime',)

    @property
    def model_instance(self):
        if self.model == "workspace_thread":
            return self.workspace_thread
        elif self.model == "workspace_reply":
            return self.workspace_reply

    def __str__(self):
        return self.text

    def save(self, send_email=True, **kwargs):
        super().save(**kwargs)
        if send_email:
            current_site = "http://localhost:8000" #get_current_site(self.request)
            subject = 'New Notification from Semety'
            message = render_to_string('email/notification.html', {
                'user': self.user,
                'notification': self,
                'domain': current_site, #.domain,
                #'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                #'token': account_activation_token.make_token(user),
            })
            self.user.email_user(subject, message)
