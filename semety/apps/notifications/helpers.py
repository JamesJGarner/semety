from .models import Notification

def notify(user, created, model, model_instance):

    if model_instance.user == user:
        return

    note = Notification(
        user=user,
        model = model
    )

    if model == "workspace_thread":
        if created:
            note.text = "<b>{}</b> assigned <b>{}</b> to you.".format(model_instance.user, model_instance.title)
        else:
           note.text = "<b>{}</b> updated <b>{}</b>".format(model_instance.user, model_instance.title)

        note.workspace_thread = model_instance
        note.icon = "fas fa-comments"
        note.model = model
    elif model == "workspace_reply":
        if created:
            note.text = "<b>{}</b> replied to <b>{}</b>.".format(model_instance.user, model_instance.thread.title)
        else:
           note.text = "<b>{}</b> updated a reply in <b>{}</b>".format(model_instance.user, model_instance.thread.title)

        note.workspace_reply = model_instance
        note.icon = "fas fa-reply"

    note.save()
    #TODO send email
