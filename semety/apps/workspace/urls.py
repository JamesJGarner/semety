from django.urls import path
from .views import *

app_name = 'workspace'

urlpatterns = [
    path('workspace/', TabList.as_view(), name='tab_list'),

    path('workspace/manage/', TabManage.as_view(), name='tab_manage'),
    path('workspace/manage/create/', TabCreate.as_view(), name='tab_create'),
    path('workspace/manage/<slug:slug>/update/', TabUpdate.as_view(), name='tab_update'),
    path('workspace/manage/<slug:slug>/delete/', TabDelete.as_view(), name='tab_delete'),


    path('workspace/<slug:slug>/thread/', RedirectTab.as_view(), name="original_view"),
    path('workspace/<slug:slug>/', TabDetail.as_view(), name='tab_detail'),

    path('workspace/<slug:tab>/thread/<slug:slug>/', ThreadDetail.as_view(), name='thread_detail'),
    path('workspace/<slug:tab>/thread/<slug:slug>/edit/', ThreadEdit.as_view(), name='thread_update'),
    path('workspace/<slug:tab>/thread/<slug:slug>/edit_message/', ThreadEditMessage.as_view(), name='thread_update_message'),
    path('workspace/<slug:tab>/thread/<slug:slug>/delete/', ThreadDelete.as_view(), name='thread_delete'),
    path('workspace/<slug:tab>/add-thread/', ThreadCreate.as_view(), name='thread_create'),

    path('workspace/<slug:tab>/thread/<slug:thread>/reply/', ReplyCreate.as_view(), name='thread_reply'),
    path('workspace/<slug:tab>/thread/<slug:thread>/reply/<int:pk>/edit/', ReplyEdit.as_view(), name='reply_update'),
    path('workspace/<slug:tab>/thread/<slug:thread>/reply/<int:pk>/delete/', ReplyDelete.as_view(), name='reply_delete'),

]
