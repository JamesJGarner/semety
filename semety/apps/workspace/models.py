
from django.db import models
from django.contrib.auth import get_user_model
from semety.apps.core.models import Company
from django.template.defaultfilters import slugify
from .slug_helpers import unique_slugify
from django.dispatch import receiver
from django.db.models.signals import post_save


User = get_user_model()

class Status(models.Model):

    name = models.CharField(
        max_length=200,
    )

    color = models.CharField(
        max_length=200,
        null=True,
    )

    team = models.ForeignKey(
        Company,
        null=True,
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.name

    @property
    def css_class(self):
        return "builtin-status-" + slugify(self.name)

    @property
    def css_class_border(self):
        return self.css_class + "-border"

    @property
    def css_class_background(self):
        return self.css_class + "-background"

    @property
    def css_class_color(self):
        return self.css_class + "-color"


class Tab(models.Model):

    name = models.CharField(
        max_length=200,
    )

    icon = models.CharField(
        max_length=200,
        blank=True
        )

    team = models.ForeignKey(
        Company,
        null=True,
        on_delete=models.PROTECT,
    )

    created_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
    )

    datetime_created = models.DateTimeField(
        auto_now_add=True,
        blank=True,
        null=True,
    )

    slug = models.SlugField()

    def save(self, **kwargs):
        unique_slugify(self, self.name)
        super().save(**kwargs)
        self.created_by.companyuser.access.add(self)

    @models.permalink
    def get_absolute_url(self):
            return ("workspace:tab_detail", (), {
                "slug": self.slug
            })

    def __str__(self):
        return self.name

    def last_activity(self):
        threads = Thread.objects.filter(tab=self)

        if threads:
            thread = threads.latest("datetime")
            replies =  Reply.objects.filter(thread__in=threads)
            if replies:
                reply = replies.latest("datetime")
                if thread.datetime > reply.datetime:
                    return thread
                else:
                    return reply
            else:
                return thread
        else:
            return None

    @models.permalink
    def get_update_url(self):
        return ("workspace:tab_update", (), {
            "slug": self.slug,
        })

    @property
    def reveal(self):
        return "reveal_modal_tab" + str(self.pk)

    @property
    def form(self):
        from .forms import TabForm
        return TabForm(instance=self)


class Thread(models.Model):

    tab = models.ForeignKey(
        Tab,
        on_delete=models.CASCADE,
        )

    user = models.ForeignKey(
        User,
        related_name="created_by",
        on_delete=models.PROTECT,
    )

    status = models.ForeignKey(
        Status,
        on_delete=models.PROTECT,
    )

    title = models.CharField(
        max_length=200,
        null=True,
    )

    text = models.TextField(
        null=True,
    )

    assigned = models.ForeignKey(
        User,
        blank=True,
        null=True,
        on_delete=models.PROTECT,
    )

    datetime = models.DateTimeField(
        auto_now_add=True,
        null=True
        )

    last_updated = models.DateTimeField(
        null=True,
        auto_now=True,
    )

    slug = models.SlugField()

    @models.permalink
    def message_update_url(self):
        return ("workspace:thread_update_message", (), {
            "tab": self.tab.slug,
            "slug": self.slug
        })

    @models.permalink
    def get_absolute_url(self):
            return ("workspace:thread_detail", (), {
                "tab": self.tab.slug,
                "slug": self.slug
            })

    class Meta:
        ordering = ['-pk']

    def __str__(self):
        return str(self.tab) + ' - ' + str(self.title)

    @property
    def reveal(self):
        return "reveal_modal_thread" + str(self.pk)

    @property
    def revealSemetyText(self):
        return "reveal_modal_thread_semety" + str(self.pk)

    @property
    def form(self):
        from .forms import ThreadMessageForm
        return ThreadMessageForm(instance=self)

    def replies(self):
        return Reply.objects.filter(thread=self)

    def save(self, changed_last_update=False, **kwargs):
        if changed_last_update:
            self.changed_last_update = True
        else:
            self.changed_last_update = False
            unique_slugify(self, self.title)
        super().save(**kwargs)

    @property
    def files(self):
        return self.threadfile_set.all()

class Reply(models.Model):

    thread = models.ForeignKey(
        Thread,
        on_delete=models.CASCADE,
    )

    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
    )

    text = models.TextField(
        null=True,
    )

    datetime = models.DateTimeField(
        auto_now_add=True,
        null=True
    )

    @property
    def form(self):
        from .forms import ReplyForm
        return ReplyForm(instance=self)

    @models.permalink
    def message_update_url(self):
        return ("workspace:reply_update", (), {
            "tab": self.thread.tab.slug,
            "thread": self.thread.slug,
            "pk": self.pk,

        })

    @property
    def revealSemetyText(self):
        return "reveal_modal_reply_semety" + str(self.pk)

    @property
    def reveal(self):
        return "reveal_modal_reply" + str(self.pk)

    @models.permalink
    def get_absolute_url(self):
            return ("workspace:thread_detail", (), {
                "tab": self.thread.tab.slug,
                "slug": self.thread.slug
            })

    @models.permalink
    def get_delete_url(self):
            return ("workspace:reply_delete", (), {
                "tab": self.thread.tab.slug,
                "thread": self.thread.slug,
                "pk": self.pk,
            })

    def save(self, **kwargs):
        super().save(**kwargs)
        self.thread.last_updated = self.datetime
        self.thread.save(changed_last_update=True)


class ThreadFile(models.Model):

    thread = models.ForeignKey(
        Thread,
        on_delete=models.CASCADE,
    )

    file = models.FileField(
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )

    datetime = models.DateTimeField(
        auto_now_add=True,
    )

@receiver(post_save, sender=Thread)
def thread_notification(sender, instance, created, **kwargs):
        from ..notifications.helpers import notify
        if not instance.changed_last_update:
            notify(user=instance.assigned, created=created, model="workspace_thread", model_instance=instance)


@receiver(post_save, sender=Reply)
def reply_notification(sender, instance, created, **kwargs):
        from ..notifications.helpers import notify
        notify(user=instance.thread.user, created=created, model="workspace_reply", model_instance=instance)
        notify(user=instance.thread.assigned, created=created, model="workspace_reply", model_instance=instance)

