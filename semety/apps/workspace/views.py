from .models import Tab, Thread, Reply
from django.views.generic import DetailView, CreateView, ListView, UpdateView, DeleteView
from django.views.generic.edit import ModelFormMixin

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic.base import RedirectView
from django.urls import reverse_lazy
from django.http import Http404
from .forms import TabForm, ThreadForm, ReplyForm, ThreadMessageForm, ThreadFileFormSet
from django.db.models import Count
from django.urls import reverse

class TabSecureQueryset():

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(pk__in=(i.pk for i in self.request.user.tabs))



class ThreadSecureQueryset():



    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(pk__in=(i.pk for i in self.request.user.threads))


class ReplySecureQueryset():

    def has_view_permission(self):
        return False

    def get_object(self):
        print('test?')
        queryset = super().get_queryset()
        return queryset.filter(pk__in=(i.pk for i in self.request.user.threads))



class TabList(TabSecureQueryset, ListView):
    model = Tab


class TabManage(TabSecureQueryset, ListView):
    model = Tab
    template_name = "workspace/tab_manage.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tab_form'] = TabForm
        return context

    def get_template_names(self):
        return "workspace/tab_manage.html"


class TabCreate(CreateView):
    model = Tab
    form_class = TabForm
    success_url = "/workspace/manage/"

    def form_valid(self, form):
        Form = form.save(commit=False)
        Form.team = self.request.user.company
        Form.created_by = self.request.user
        self.object = form.save()
        return super(ModelFormMixin, self).form_valid(form)


class TabDetail(TabSecureQueryset, DetailView):
    model = Tab
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tab_form'] = TabForm(instance=self.object)
        context['thread_form'] = ThreadForm
        order_by = self.request.GET.get('order_by')
        order = self.request.GET.get('order')

        if not order_by:
            order_by = "name"

        if not order:
            order = "ascending"

        if order == "ascending":
            extra = ""
        else:
            extra = "-"

        results = Thread.objects.filter(tab__slug=self.kwargs['slug'])
        if order_by == "name":
            results = results.order_by(extra + "title")
        elif order_by == "assigned":
            results = results.order_by(extra + "assigned")
        elif order_by =="replies":
            results = results.annotate(
                replies_count=Count("reply"),
            ).order_by(extra + 'replies_count')
        elif order_by == "published":
            results = results.order_by(extra + "last_updated")
        elif order_by == "status":
            results = results.order_by(extra + "status")

        context['order_by'] = order_by
        context['order'] = order

        paginator = Paginator(results, 10) # Show 10 replies per page

        page = self.request.GET.get('page')
        try:
            results = paginator.page(page)
        except PageNotAnInteger:
            results = paginator.page(1)
        except EmptyPage: results = paginator.page(paginator.num_pages)
        context['thread_list'] = results
        return context
        return context


class TabUpdate(TabSecureQueryset, UpdateView):
    model = Tab
    form_class = TabForm
    success_url = "/workspace/manage/"

class TabDelete(TabSecureQueryset, DeleteView):
    model = Tab
    success_url = "/workspace/manage/"

class ThreadDelete(ThreadSecureQueryset, DeleteView):
    model = Thread

    def get_success_url(self):
        return reverse('workspace:tab_detail', kwargs={'slug': self.kwargs['tab']})

class ThreadEditMessage(ThreadSecureQueryset, UpdateView):
    model = Thread
    form_class = ThreadMessageForm


class ReplyDelete(DeleteView):
    model = Reply

    def get_success_url(self):
        return reverse('workspace:thread_detail', kwargs={'tab': self.kwargs['tab'], 'slug': self.kwargs['thread']})


class ReplyEdit(UpdateView):
    model = Reply
    form_class = ReplyForm

class ThreadDetail(ThreadSecureQueryset, DetailView):
    model = Thread

    def get_context_data(self, **kwargs):
        context = super(ThreadDetail, self).get_context_data(**kwargs)
        context["category"] = self.object
        context["object"] = self.object
        context["thread_form"] = ThreadForm(instance=self.object)
        context["thread_inline_form"] = ThreadFileFormSet()

        reply_list = Reply.objects.filter(thread=self.object.pk)
        paginator = Paginator(reply_list, 10) # Show 10 replies per page

        page = self.request.GET.get('page')
        try:
            replies = paginator.page(page)
        except PageNotAnInteger:
            replies = paginator.page(1)
        except EmptyPage: replies = paginator.page(paginator.num_pages)
        context["reply_list"] = replies
        return context

    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Http404:
            raise Http404

        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class ThreadEdit(ThreadSecureQueryset, UpdateView):
    model = Thread
    fields = ["status", "title", "text", "assigned"]


class ThreadCreate(ThreadSecureQueryset, CreateView):
    model = Thread
    fields = ["status", "title", "text", "assigned"]

    def form_valid(self, form):
        Form = form.save(commit=False)
        Form.tab = Tab.objects.get(slug=self.kwargs['tab'])
        Form.user = self.request.user
        self.object = form.save()
        return super(ModelFormMixin, self).form_valid(form)



class ReplyCreate(ReplySecureQueryset, CreateView):
    model = Reply
    fields = ["text"]

    def form_valid(self, form):
        Form = form.save(commit=False)
        Form.thread = Thread.objects.get(slug=self.kwargs['thread'])
        Form.user = self.request.user
        self.object = form.save()
        return super(ModelFormMixin, self).form_valid(form)


class RedirectTab(TabSecureQueryset, RedirectView):
    def get_redirect_url(self, slug):
        return reverse_lazy('workspace:tab_detail', kwargs={'slug': slug})
