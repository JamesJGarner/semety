from django import forms
from .models import Tab, Thread, Status, Reply, ThreadFile
from django.forms.models import inlineformset_factory


class ReplyForm(forms.ModelForm):

    class Meta:
        model = Reply
        fields = ["text"]


class TabForm(forms.ModelForm):

    class Meta:
        model = Tab
        fields = ["name", "icon"]



ThreadFileFormSet = inlineformset_factory(
    Thread,
    ThreadFile,
    fields = ('file',),
    extra=1,
    can_delete=True
)

class ThreadForm(forms.ModelForm):

    status = forms.ModelChoiceField(
        queryset=Status.objects.all(),
        widget=forms.RadioSelect(),
        empty_label=None
    )

    class Meta:
        model = Thread
        fields = ["title", "text", "status", "assigned"]


class ThreadMessageForm(forms.ModelForm):

    class Meta:
        model = Thread
        fields = ["text"]
