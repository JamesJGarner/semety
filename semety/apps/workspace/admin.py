from django.contrib import admin
from .models import Status, Tab, Thread, Reply, ThreadFile

admin.site.register(Status)
admin.site.register(Tab)


class ReplyInline(admin.StackedInline):
    model = Reply

class ThreadFileInline(admin.StackedInline):
    model = ThreadFile

class ThreadAdmin(admin.ModelAdmin):
    model = Thread
    inlines = (ReplyInline, ThreadFileInline)


admin.site.register(Thread, ThreadAdmin)
