from semety.apps.core.models import Company, CompanyUser, Invite
from semety.apps.workspace.models import Thread, Tab
from rest_framework import generics
from rest_framework import serializers, viewsets
#from semety.apps.teams.views import get_team

class TabSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tab
        fields = ('name', 'icon')

class ThreadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thread
        fields = ('title','assigned', 'text', 'status')


class InviteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invite
        fields = ('invite_from','team', 'invite_to')


class ThreadList(viewsets.ModelViewSet):
    serializer_class = ThreadSerializer
    queryset = Thread.objects.all()


class TabList(viewsets.ModelViewSet):
    serializer_class = TabSerializer
    queryset = Tab.objects.all()
    def get_queryset(self):
        """
        This view should return a list of all the threads
        for the currently authenticated user.
        """
        return self

class InviteList(viewsets.ModelViewSet):
    serializer_class = InviteSerializer
    queryset = Invite.objects.all()


    def get_queryset(self):
        return Invite.objects.filter(team=get_team(self.request.user))
