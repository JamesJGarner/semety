from rest_framework import routers
from django.conf.urls import url, include
from .serializers import ThreadList, TabList, InviteList

app_name = 'api'
# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'tabs', TabList)
router.register(r'threads', ThreadList)
router.register(r'invites', InviteList)


urlpatterns = [
	url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
