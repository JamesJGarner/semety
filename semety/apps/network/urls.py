from .views import *
from django.conf.urls import url

app_name = "network"

urlpatterns = [
    url(r'^profile/$', UserProfile.as_view(), name="profile"),
    url(r'^profile/update/$', UserProfileUpdate.as_view(), name="profile_update"),
    url(r'^$', CompanyDetail.as_view(), name="company_detail"),
    url(r'^update/$', CompanyUpdate.as_view(), name="company_update"),
    url(r'^(?P<pk>\d+)/update/$', CompanyUserUpdate.as_view(), name="company_user_update"),
    url(r'^invite/$', CompanyInviteForm.as_view(), name="invite_create"),
    #url(r'^invite/(?P<pk>\d+)/$', CompanyInviteDetail.as_view(), name="invite_detail"),
    url(r'^invite/(?P<pk>\d+)/edit/$', CompanyInviteFormUpdate.as_view(), name="invite_edit"),
    url(r'^invite/(?P<pk>\d+)/delete/$', CompanyInviteFormDelete.as_view(), name="invite_delete"),
]
