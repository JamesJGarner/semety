from django.views.generic import DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth import get_user_model
from ..core.models import Invite, Company, CompanyUser
from ..core.forms import UserForm, CompanyUserForm
from django.urls import reverse_lazy
from ..core.forms import UserInForm

User = get_user_model()


class UserProfile(DetailView):
    model = User

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = UserForm(instance=self.object)
        return context

    def get_object(self):
        queryset = self.get_queryset()
        return queryset.get(pk=self.request.user.pk)


class UserProfileUpdate(UpdateView):
    model = User
    form_class = UserForm
    success_url = reverse_lazy('network:profile')

    def get_object(self):
        queryset = self.get_queryset()
        return queryset.get(pk=self.request.user.pk)


class CompanyDetail(DetailView):
    model = Company

    def get_object(self):
        queryset = self.get_queryset()
        return queryset.get(pk=self.request.user.company.pk)


class CompanyUpdate(UpdateView):
    model = Company
    fields = "__all__"
    success_url = reverse_lazy('network:company_detail')

    def get_object(self):
        queryset = self.get_queryset()
        return queryset.get(pk=self.request.user.company.pk)


class CompanyUserUpdate(UserInForm, UpdateView):
    model = CompanyUser
    form_class = CompanyUserForm
    success_url = reverse_lazy('network:company_detail')

class CompanyInviteForm(CreateView):
    model = Invite
    fields = ('invite_to',)

    def form_valid(self, form):
        temp = form.save(commit=False)
        temp.invite_from = self.request.user
        temp.company = self.request.user.company
        temp.code = "testcode"
        return super().form_valid(form)


class CompanyInviteFormUpdate(UpdateView):
    model = Invite
    fields = ('invite_to',)


class CompanyInviteFormDelete(DeleteView):
    model = Invite
    success_url = reverse_lazy('network:company_detail')


