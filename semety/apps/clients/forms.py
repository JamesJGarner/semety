from django import forms
from .models import Client, Contact, ContactMemo


class ClientForm(forms.ModelForm):

    class Meta:
        model = Client
        fields = ["name", "main_phone", "main_email", "logo", "address_line_1", "city", "county", "postal_code", "country"]


class ContactForm(forms.ModelForm):

    class Meta:
        model = Contact
        fields = ['full_name', 'email', 'phone']

class ContactMemoForm(forms.ModelForm):

    class Meta:
        model = ContactMemo
        fields = ['title', 'text']
