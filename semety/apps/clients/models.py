from django.db import models
from semety.apps.core.models import Company
from sorl.thumbnail import ImageField



class AddressAbstract(models.Model):

    address_line_1 = models.CharField(
        max_length=200,
        null=True,
    )

    city = models.CharField(
        max_length=200,
        null=True,
    )

    county = models.CharField(
        max_length=200,
        null=True,
    )

    postal_code = models.CharField(
        max_length=200,
        null=True,
    )

    country = models.CharField(
        max_length=200,
        null=True,
    )


    def full_address(self):
        return self.address_line_1 + '\n' + self.city + '\n' + self.county + '\n' + self.postal_code + '\n' + self.country


    class Meta:
        abstract =True


class Client(AddressAbstract):

    name = models.CharField(
        max_length=200,
    )

    main_phone = models.CharField(
        max_length=200,
    )

    main_email = models.EmailField(
        max_length=200,
    )

    logo = ImageField(
        null=True,
        blank=True,
    )

    team = models.ForeignKey(
        Company,
        on_delete=models.PROTECT,
    )

    class Meta:
        ordering = ['name']

    @models.permalink
    def get_absolute_url(self):
            return ("clients:client_detail", (), {
                "pk": self.pk
            })


    @models.permalink
    def get_delete_url(self):
            return ("clients:client_delete", (), {
                "pk": self.pk,
            })


class Contact(models.Model):

    client = models.ForeignKey(
        Client,
        blank=True,
        on_delete=models.CASCADE,
    )

    full_name = models.CharField(
        max_length=200,
        null=True,
        )


    email = models.EmailField(
        max_length=400,
    )

    phone = models.CharField(
        max_length=300,
    )

    memo = models.TextField(
        null=True,
    )

    @property
    def memos(self):
        return self.contactmemo_set.all()


    @models.permalink
    def get_delete_url(self):
            return ("clients:contact_delete", (), {
                "client": self.client.pk,
                "pk": self.pk,
            })

    @models.permalink
    def get_absolute_url(self):
            return ("clients:contact_detail", (), {
                "client": self.client.pk,
                "pk": self.pk,
            })


class ContactMemo(models.Model):

    contact = models.ForeignKey(
        Contact,
        on_delete=models.CASCADE,
    )

    title = models.CharField(
        max_length=200,
        null=True,
    )

    text = models.TextField(
        null=True,
    )

    datetime = models.DateTimeField(
        auto_now_add=True,
        null=True
    )

    @models.permalink
    def get_absolute_url(self):
            return ("clients:contact_detail", (), {
                "client": self.contact.client.pk,
                "pk": self.contact.pk,
            })


    @models.permalink
    def get_update_url(self):
            return ("clients:contact_memo_update", (), {
                "client": self.contact.client.pk,
                "contact": self.contact.pk,
                "pk": self.pk,
            })

    @models.permalink
    def get_delete_url(self):
            return ("clients:contact_memo_delete", (), {
                "client": self.contact.client.pk,
                "contact": self.contact.pk,
                "pk": self.pk,
            })

    @property
    def modal(self):
        return "memo_modal_" + str(self.pk)

    @property
    def plain_text(self):
        text = self.text
        tags_allowed = ["<h1>", "</h1>", "<h2>", "</h2>", "<h3>", "</h3>", "<h4>", "<h5>", "<h6>", "</h6>", "<div>", "</div>", "<br>", "<b>", "</b>", "<i>", "</i>", "<u>", "</u>"]
        for x in tags_allowed:
            text = text.replace(x, "")
        return text

    @property
    def revealSemetyText(self):
        return "revealSemetyText_memo_" + str(self.pk)

    @property
    def form(self):
        from .forms import ContactMemoForm
        return ContactMemoForm(instance=self)


