# Generated by Django 2.0.6 on 2019-02-02 21:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('clients', '0002_client_team'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactmemo',
            name='contact',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clients.Contact'),
        ),
    ]
