from .models import Client, Contact, ContactMemo
from django.views.generic import DetailView, CreateView, ListView, UpdateView, DeleteView
from django.views.generic.edit import ModelFormMixin
from .forms import ClientForm, ContactForm, ContactMemoForm
from django.urls import reverse





def GetUser(teammate):
    return teammate.user

class ClientList(ListView):
    model = Client

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['client_add_form'] = ClientForm
        return context

    def get_queryset(self):
        return Client.objects.filter(team=self.request.user.company)


class ClientDetail(DetailView):
    model = Client

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['client_edit_form'] = ClientForm(instance=self.object)
        context['add_contact_form'] = ContactForm
        return context


class ClientCreate(CreateView):
    model = Client
    form_class = ClientForm

    def form_valid(self, form):
        Form = form.save(commit=False)
        Form.user = self.request.user
        Form.team = self.request.user.company
        self.object = form.save()
        return super(ModelFormMixin, self).form_valid(form)


class ClientUpdate(UpdateView):
    model = Client
    form_class = ClientForm


class ClientDelete(DeleteView):
    model = Client

    def get_success_url(self):
        return reverse('clients:client_list', kwargs={})


#Contacts

class ContactDetail(DetailView):
    model = Contact

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['memo_form'] = ContactMemoForm
        context['contact_edit_form'] = ContactForm(instance=self.object)
        return context


class ContactDelete(DeleteView):
    model = Contact

    def get_success_url(self):
        return reverse('clients:client_detail', kwargs={'pk': self.kwargs['client']})


class ContactMemoUpdate(UpdateView):
    model = ContactMemo
    form_class = ContactMemoForm


class ContactMemoDelete(DeleteView):
    model = ContactMemo

    def get_success_url(self):
        return reverse('clients:contact_detail', kwargs={'client': self.kwargs['client'], 'pk': self.kwargs['contact']})


class ContactMemo(CreateView):
    model = ContactMemo
    form_class = ContactMemoForm

    def form_valid(self, form):
        Form = form.save(commit=False)
        Form.contact = Contact.objects.get(pk=self.kwargs['pk'])
        self.object = form.save()
        return super(ModelFormMixin, self).form_valid(form)


class ContactCreate(CreateView):
    model = Contact
    form_class = ContactForm

    def form_valid(self, form):
        Form = form.save(commit=False)
        Form.client = Client.objects.get(pk=self.kwargs['client'])
        self.object = form.save()
        return super(ModelFormMixin, self).form_valid(form)


class ContactUpdate(UpdateView):
    model = Contact
    form_class = ContactForm
