from django.urls import path
from .views import *

app_name = 'contacts'

urlpatterns = [
    path('', ClientList.as_view(), name='client_list'),
    path('add/', ClientCreate.as_view(), name='client_add'),
    path('<int:pk>/', ClientDetail.as_view(), name='client_detail'),
    path('<int:pk>/update/', ClientUpdate.as_view(), name='client_update'),
    path('<int:pk>/delete/', ClientDelete.as_view(), name='client_delete'),


    path('<int:client>/contact/add/', ContactCreate.as_view(), name='contact_add'),
    path('<int:client>/contact/<int:pk>/', ContactDetail.as_view(), name='contact_detail'),
    path('<int:client>/contact/<int:pk>/update/', ContactUpdate.as_view(), name='contact_update'),
    path('<int:client>/contact/<int:pk>/delete/', ContactDelete.as_view(), name='contact_delete'),


    path('<int:client>/contact/<int:pk>/add-memo/', ContactMemo.as_view(), name='contact_memo'),
    path('<int:client>/contact/<int:contact>/<int:pk>/update/', ContactMemoUpdate.as_view(), name='contact_memo_update'),
    path('<int:client>/contact/<int:contact>/<int:pk>/delete/', ContactMemoDelete.as_view(), name='contact_memo_delete'),

]
