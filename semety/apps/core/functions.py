from .models import CompanyUser, Invite, Company
from .tokens import account_activation_token as company_invite_token


def get_company(user):
    try:
        companyuser = CompanyUser.objects.get(user=user)
        return Company.objects.get(pk=companyuser.company.pk)
    except:
        company = Company.objects.create()
        CompanyUser.objects.create(company=company, user=user)
        return company


def is_in_company(user, company):
    return bool(CompanyUser.objects.filter(company=company, user=user))


def is_user_admin(user, company):
    return bool(CompanyUser.objects.filter(company=company, user=user, admin=True))


def add_user_to_company(user, company, admin=False):
    return CompanyUser.objects.create(user=user, company=company, admin=admin)


def invite_user(company, invite_from, invite_to):
    if not is_in_company(invite_from, company):
        raise Exception('You need to be a member of this company.')

    if not is_user_admin(invite_from, company):
        raise Exception('You need to be a admin of this company to invite.')

    return Invite.objects.create(
        invite_from=invite_from,
        company=company,
        invite_to=invite_to,
        code=company_invite_token.make_token(invite_from)
    )
