import re

from django import template
from django.urls import reverse, NoReverseMatch
from datetime import datetime
register = template.Library()


@register.simple_tag(takes_context=True)
def active(context, pattern_or_urlname, kwargs=None):
    try:
        if pattern_or_urlname != "frontend:homepage":
            extra = ""
        else:
            extra = "$"
        if kwargs is not None:
            pattern = '^' + reverse(pattern_or_urlname, kwargs={'pk': kwargs}) + extra
        else:
            pattern = '^' + reverse(pattern_or_urlname) + extra
    except NoReverseMatch:
        pattern = "pattern_or_urlname"
    path = context['request'].path
    if re.search(pattern, path):
        return 'active'
    return ''


@register.simple_tag(takes_context=True)
def date(context):
    return datetime.now().date()
