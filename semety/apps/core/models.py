from __future__ import unicode_literals

from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.dispatch import receiver
from django.db.models.signals import post_save

from .managers import UserManager
from sorl.thumbnail import ImageField


class UserPaymentModel(models.Model):

    stripe_id = models.CharField(
        max_length=400,
        blank=True,
    )

    # This is stored on the Stripe Card object as name.
    payer_name = models.CharField(
        max_length=100,
        blank=True,
        null=True,
    )

    # MMYYYY (or MYYYY)
    #
    # Month: payer_last_4[:-4]
    # Year: payer_last_4[-4:]

    payer_expiry = models.CharField(
        max_length=6,
        blank=True,
        null=True,
    )

    payer_last_4 = models.CharField(
        max_length=4,
        blank=True,
        null=True,
    )

    postal_address_line_1 = models.CharField(
        "address line 1",
        max_length=100,
        blank=True,
        null=True,
    )

    postal_address_line_2 = models.CharField(
        "address line 2",
        max_length=100,
        blank=True,
        null=True,
    )

    postal_address_city = models.CharField(
        "town / city",
        max_length=100,
        blank=True,
        null=True,
    )

    postal_address_postcode = models.CharField(
        "postcode",
        max_length=100,
        blank=True,
        null=True,
    )

    billing_address_same = models.BooleanField(
        default=True,
    )

    billing_address_line_1 = models.CharField(
        "address line 1",
        max_length=100,
        blank=True,
        null=True,
    )

    billing_address_line_2 = models.CharField(
        "address line 2",
        max_length=100,
        blank=True,
        null=True,
    )

    billing_address_city = models.CharField(
        "town / city",
        max_length=100,
        blank=True,
        null=True,
    )

    billing_address_postcode = models.CharField(
        "postcode",
        max_length=100,
        blank=True,
        null=True,
    )

    class Meta:
        abstract = True


class AddressAbstract(models.Model):

    address_line_1 = models.CharField(
        max_length=200,
        null=True,
    )

    city = models.CharField(
        max_length=200,
        null=True,
    )

    county = models.CharField(
        max_length=200,
        null=True,
    )

    postal_code = models.CharField(
        max_length=200,
        null=True,
    )

    country = models.CharField(
        max_length=200,
        null=True,
    )

    class Meta:
        abstract = True


class Company(AddressAbstract, UserPaymentModel):

    name = models.CharField(
        max_length=200,
    )

    logo = ImageField(
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name


class User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(
        _('email address'),
        unique=True
    )

    profile_picture = ImageField(
        null=True,
        blank=True,
    )

    first_name = models.CharField(
        _('first name'),
        max_length=30,
        blank=True
    )

    last_name = models.CharField(
        _('last name'),
        max_length=30,
        blank=True
    )

    date_joined = models.DateTimeField(
        _('date joined'),
        auto_now_add=True
    )

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )

    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    email_confirmed = models.BooleanField(
        default=False,
    )

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject=subject, html_message=message, message=message, from_email="noreply@semety.com", recipient_list=[self.email], **kwargs)


    def __str__(self):
        return self.get_full_name()

    @property
    def company(self):
        try:
            return self.companyuser.company
        except:
            return None

    @property
    def tabs(self):
        return self.companyuser.access.all()

    @property
    def threads(self):
        for tab in self.companyuser.access.all():
            for thread in tab.thread_set.all():
                yield thread


    @property
    def notifications(self):
        from ..notifications.models import Notification
        return Notification.objects.filter(user=self)

    @property
    def unread_notifications(self):
        return self.notifications.filter(read=False)

    @property
    def status(self):
        from ..workspace.models import Status
        return Status.objects.all()

@receiver(post_save, sender=User)
def create_company_user(sender, instance, created, **kwargs):
    if created:
        company = Company.objects.create(name="Company Name")
        CompanyUser.objects.create(
            user=instance,
            company=company,
            admin=True,
        )


class CompanyUser(models.Model):

    company = models.ForeignKey(
        Company,
        on_delete=models.PROTECT
    )

    user = models.OneToOneField(
        User,
        on_delete=models.PROTECT
    )

    job_title = models.CharField(
        max_length=50,
        blank=True,
        null=True,
    )

    admin = models.BooleanField(
        default=False,
    )

    access = models.ManyToManyField(
        'workspace.Tab',
    )

    @models.permalink
    def get_update_url(self):
        return ("network:company_user_update", (), {
            "pk": self.pk,
        })

    @property
    def reveal(self):
        return "reveal_modal_companyuser" + str(self.pk)

    @property
    def form(self):
        from .forms import CompanyUserForm
        return CompanyUserForm(instance=self, user=self.user)


class Invite(models.Model):

    invite_from = models.ForeignKey(
        User,
        on_delete=models.PROTECT
    )

    invite_to = models.EmailField(
        max_length=400,
        null=True,
    )

    company = models.ForeignKey(
        Company,
        on_delete=models.PROTECT,
        null=True
    )

    code = models.CharField(
        max_length=400,
        null=True,
        blank=True,
    )

    datetime = models.DateTimeField(
        auto_now_add=True
    )

    def get_absolute_url(self):
        return reverse('network:invite_detail', kwargs={'pk': self.pk})
