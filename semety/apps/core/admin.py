from django.contrib import admin
from .models import User, Company, CompanyUser, Invite

admin.site.register(Company)
admin.site.register(User)

admin.site.register(CompanyUser)
admin.site.register(Invite)
