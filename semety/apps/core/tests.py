from django.test import TestCase
from .models import Company, Invite
from .functions import is_user_admin, get_company, is_in_company, add_user_to_company, invite_user
from django.contrib.auth import get_user_model

User = get_user_model()


class TestNonAdminCompanyUser(TestCase):

    def setUp(self):
        self.company = Company.objects.create()
        self.company_2 = Company.objects.create()
        self.user = User.objects.create()
        self.company_mate = add_user_to_company(user=self.user, company=self.company, admin=False)

    def test_admin_function(self):
        self.assertEqual(is_user_admin(user=self.user, company=self.company), False)

    def test_get_company_function(self):
        self.assertEqual(get_company(user=self.user), self.company)

    def test_is_in_company_function(self):
        self.assertEqual(is_in_company(user=self.user, company=self.company), True)

    def test_is_in_company_2_function(self):
        self.assertEqual(is_in_company(user=self.user, company=self.company_2), False)


class TestAdminCompanyUser(TestCase):

    def setUp(self):
        self.company = Company.objects.create()
        self.user = User.objects.create()
        self.company_mate = add_user_to_company(user=self.user, company=self.company, admin=True)

    def test_admin_function(self):
        self.assertEqual(is_user_admin(company=self.company, user=self.user), True)


class TestInivteCompanyUser(TestCase):

    def setUp(self):
        self.company = Company.objects.create()
        self.user = User.objects.create(email="1")
        self.user_1 = User.objects.create(email="2")
        self.company_mate = add_user_to_company(user=self.user, company=self.company, admin=True)

    def test_invite(self):
        self.invite = invite_user(company=self.company, invite_from=self.user, invite_to=self.user_1)
        self.assertEqual(self.invite, Invite.objects.get(company=self.company, invite_from=self.user, invite_to=self.user_1))


class TestExceptionInvites(TestCase):

    def setUp(self):
        self.company = Company.objects.create()
        self.user = User.objects.create(email="1")
        self.user_1 = User.objects.create(email="2")
        self.company_mate = add_user_to_company(user=self.user, company=self.company, admin=False)

    def test_invite_not_in_company(self):
        with self.assertRaises(Exception):
            invite_user(company=self.company, invite_from=self.user_1, invite_to=self.user)

    def test_invite_not_admin(self):
        with self.assertRaises(Exception):
            invite_user(company=self.company, invite_from=self.user, invite_to=self.user_1)
