from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UsernameField
from django.contrib.auth import get_user_model
from .models import CompanyUser
from ..workspace.models import Tab

User = get_user_model()

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1', 'password2', )


class UserInForm():

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class CompanyFormValid():

    def form_valid(self, form):
        temp = form.save(commit=False)
        temp.company = self.request.user.company
        return super().form_valid(form)

class LoginForm(AuthenticationForm):

    username = UsernameField(
        max_length=254,
        widget=forms.EmailInput(attrs={'autofocus': True}),
    )


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'profile_picture')


class CompanyUserForm(forms.ModelForm):


    access = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        required=False,
        queryset=Tab.objects.none())


    class Meta:
        model = CompanyUser
        fields = ('admin', 'access')

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        if user is not None:
            self.fields['access'].queryset = Tab.objects.filter(team=user.company)
