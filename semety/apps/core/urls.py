from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.urls import path
import semety.apps.core.views as core_views

from .forms import LoginForm
from .views import *
app_name = "core"

urlpatterns = [
    url(r'^$', core_views.home, name='home'),
    url(r'^login/$', auth_views.login, {'template_name': 'core/login.html', 'authentication_form': LoginForm}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'auth:login'}, name='logout'),


    url(r'^account_activation_sent/$', core_views.account_activation_sent, name='account_activation_sent'),
    path('activate/<str:uidb64>/<str:token>',core_views.activate, name='activate'),

]
