from django.contrib.humanize.templatetags.humanize import naturaltime
from datetime import datetime, timezone
from django import template

register = template.Library()


@register.filter
def naturaltime_recent_only(datetime_object):
    time_between_insertion = datetime.now(timezone.utc) - datetime_object
    if time_between_insertion.days > 365:
        return datetime_object.strftime("%Y-%m-%d, %H:%M")
    if  time_between_insertion.days >= 1:
        return datetime_object.strftime("%b %d, %H:%M")
    else:
        return naturaltime(datetime_object)
