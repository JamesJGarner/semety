from requests import request

from django.core.files.base import ContentFile


def save_profile_picture(backend, strategy, details, response, user, *args, **kwargs):
    url = None
    if backend.name == 'facebook':
        url = "http://graph.facebook.com/%s/picture?type=large"%response['id']
    if backend.name == 'google-oauth2':
        url = response['image'].get('url')
        ext = url.split('.')[-1]
    if url:
        try:
            response = request('GET', url, params={'type': 'large'})
            response.raise_for_status()
            user.profile.profile_picture.save('{0}_social.jpg'.format(user.username), ContentFile(response.content))
            user.profile.save()
        except:
            pass
