from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

STAR_RATING = (
    ('1', '1 star'),
    ('2', '2 stars'),
    ('3', '3 stars'),
    ('4', '4 stars'),
    ('5', '5 stars'),
)


class Feedback(models.Model):

    comment = models.TextField()

    rating = models.CharField(
            max_length=1,
            choices=STAR_RATING
    )

    user = models.ForeignKey(
        User,
        null=True,
        on_delete=models.PROTECT,
    )
