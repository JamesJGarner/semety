from django.views.generic import TemplateView, CreateView
from .models import Feedback
from semety.apps.core.models import Invite
from semety.apps.workspace.models import Thread, Reply



class FeebackForm(CreateView):
    model = Feedback
    success_url = '/'
    fields = ('comment', 'rating')


    def form_valid(self, form):
        Form = form.save(commit=False)
        Form.user = self.request.user
        self.object = form.save()
        return super().form_valid(form)


class Homepage(TemplateView):
    template_name = 'site/homepage.html'

    def get_context_data(self, **kwargs):
        context = super(Homepage, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['invite_code'] = self.request.COOKIES.get('invite_code')
            #context['yournetwork'] = CompanyMate.objects.filter(team=get_team(self.request.user))
            context['notifications'] = Invite.objects.filter(invite_to=self.request.user.email)
            try:
                pass
                #teammates = Connections(self)
                #context['thread'] = Thread.objects.filter(user__in=map(GetUser, teammates)).order_by('datetime').last()
            except:
                context['thread'] = Thread.objects.filter(user=self.request.user.id).order_by('datetime').last()

            try:
                pass
                #teammates = Connections(self)
                #context['reply'] = Reply.objects.filter(user__in=map(GetUser, teammates)).order_by('datetime').last()
            except:
                context['reply'] = Reply.objects.filter(user=self.request.user.id).order_by('datetime').last()
        return context


class About(TemplateView):
    template_name = 'site/about.html'


class Legal(TemplateView):
    template_name = 'site/legal.html'


class Contact(TemplateView):
    template_name = 'site/contact.html'


class Explore(TemplateView):
    template_name = 'site/explore.html'


class WelcomePage(TemplateView):
    template_name = 'site/welcome.html'
